package com.devcamp.task56b70.Controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b70.models.Cat;
import com.devcamp.task56b70.models.Dog;

@RestController
public class AnimalAPI {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList<Cat> getCat() {
        ArrayList<Cat> cats = new ArrayList<Cat>();

        Cat cat1 = new Cat("mèo đen");
        Cat cat2 = new Cat("mèo trắng");
        Cat cat3 = new Cat("mèo");

        cats.add(cat1);
        cats.add(cat2);
        cats.add(cat3);
        return cats;
    }
    @GetMapping("/dogs")
    public ArrayList<Dog> getDog() {
        ArrayList<Dog> dogs = new ArrayList<Dog>();

        Dog dog1 = new Dog("husky");
        Dog dog2 = new Dog("alaska");
        Dog dog3 = new Dog("chó");

        dogs.add(dog1);
        dogs.add(dog2);
        dogs.add(dog3);
        return dogs;
    }

}
